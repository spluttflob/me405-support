#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" @file kbd_2.py
    This file contains code to verify that the @c keyboard module works, or at
    least sort of works, or...just doesn't completely mess up?.

    This module must be installed with a command such as
    @code
    pip3 install keyboard
    @endcode
    before use (and maybe use @c sudo or @c su if you're on Unix or Linux).

    On Linux, this program must be run as root because security.  On Windows,
    there is no security.  On MacOS, there's security but I'm not sure if it 
    must be run as root; check @c https://github.com/boppreh/keyboard for more
    information. 

    @author jr
    @date Thu Jan 14 09:43:27 2021
        
"""

import keyboard

# -----------------------------------------------------------------------------
# This example uses a state machine to keep track of keys being pressed so that
# holding a key down only results in the keypress being detected once. This
# state machine could be integrated with your vending machine state machine.
#
# On most systems, keys are "echoed" by the operating system; when you see just
# an "a" on the screen it doesn't mean the keypress was responded to by this
# program, but "<a>" does mean there was a response.
# -----------------------------------------------------------------------------

S0_WAIT_FOR_KEY = 0          # State where we wait for a key to be pressed
S9_WAIT_FOR_RELEASE = 9      # State where we wait for a key to be released

# One could also have separate states to wait for each key to be released; this
# example doesn't do that, but here are states which could be used
S1_KEY_a = 1                 # An 'a' was pressed
S2_KEY_b = 2                 # A 'b' key was pressed


if __name__ == "__main__":

    # Run a simple loop which responds to some keys and ignores others.
    # If someone presses control-C, exit this program cleanly
    state = S0_WAIT_FOR_KEY

    while True:
        try:
            # State 0: Watching the keys of interest; others are ignored
            if state == S0_WAIT_FOR_KEY:
                if keyboard.is_pressed ('a'):
                    print ('<a>')
                    state = S9_WAIT_FOR_RELEASE
                elif keyboard.is_pressed ('b'):
                    print ('<b>')
                    state = S9_WAIT_FOR_RELEASE

            # State 9: Wait for keys to be released
            elif state == S9_WAIT_FOR_RELEASE:   
                if not keyboard.is_pressed ('a') \
                   and not keyboard.is_pressed ('b'):
                    state = S0_WAIT_FOR_KEY
            else:
                print ("Error: Illegal state", state)

        # If Control-C is pressed, this is sensed separately from the keyboard
        # module; it generates an exception, and we break out of the loop
        except KeyboardInterrupt:
            break

    print ("Control-C has been pressed, so it's time to exit.")

